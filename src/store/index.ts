import { createStore, combineReducers } from "redux";
import { LOGIN, LOGOUT } from "./types";
import { newsListReducer } from "../components/NewsList/store";
import { newsItemsReducer } from "../components/NewsItem/store";

export const authReducer = (
  state: {} = {},
  action: { type: string; payload: { token: string; user: { name: string } } }
) => {
  switch (action.type) {
    case LOGIN:
      localStorage.setItem("token", action.payload.token);
      return {
        ...state,
        ...action.payload,
      };
    case LOGOUT:
      localStorage.removeItem("token");
      return {};
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  auth: authReducer,
  newsList: newsListReducer,
  newsItem: newsItemsReducer,
});

export const store = createStore(rootReducer);
