export interface credentialsType {
  email: string;
  password: string;
}

export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
