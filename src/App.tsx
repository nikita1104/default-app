import React from "react";
import "./App.scss";
import { Menu } from "./components/Menu";
import { Routes } from "./components/routes/index";
import { connect } from "react-redux";

function App({
  auth,
  logout,
}: {
  auth: { token: string | undefined };
  logout: any;
}) {
  const { token } = auth;
  return (
    <div className="App">
      <header className="header">
        <div className="container">
          {token && (
            <div className="fb sb iac">
              <Menu />
              <div>
                <button onClick={() => logout()}>logout</button>
              </div>
            </div>
          )}
        </div>
      </header>
      <div className="main">
        <div className="container">
          <Routes />
        </div>
      </div>
    </div>
  );
}
const mapStateToProps = (store: any) => ({ auth: store.auth });
const dispatchToProps = (dispatch: any) => ({
  logout: () => dispatch({ type: "LOGOUT" }),
});
export default connect(mapStateToProps, dispatchToProps)(App);
