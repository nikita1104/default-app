import React, { useEffect } from "react";
import { Pagination } from "../pagination/index";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getData } from "./store/thunks";
import { Preload } from "../UI";
export const Component = ({ store, getNewsList }: any) => {
  useEffect(() => {
    getNewsList({ page: 1 });
  }, [getNewsList]);
  const { loading, data, error } = store;
  const { pages, newsList, page } = data;

  if (loading) return <Preload />;
  if (error) return <>error</>;

  return (
    <div>
      <h1>NewsList</h1>
      <div className="news-list">
        {newsList?.map(
          (
            x: { title: string; description: string; id: number },
            i: number
          ) => (
            <div key={i} className="news-list__item">
              <Link to={`/news/${x.id}`}>
                <h3>{x.title}</h3>
                <p>{x.description}</p>
              </Link>
            </div>
          )
        )}
        <div className="news-list__pagination-area">
          <Pagination
            pageCount={pages}
            marginPagesDisplayed={5}
            pageRangeDisplayed={5}
            onPageChange={(page: number) => getNewsList({ page })}
            forcePage={page}
          />
        </div>
      </div>
    </div>
  );
};

export const NewsList = connect(
  (store: { newsList: any }) => ({ store: store.newsList }),
  (dispatch: any) => ({
    getNewsList: ({ page }: { page: number }) => {
      getData(dispatch, page);
    },
  })
)(Component);
