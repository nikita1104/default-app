import { INIT, SUCCESS, responseType } from "./types";

export const newsListReducer = (
  state: {
    loading: boolean;
    data: responseType;
  } = {
    loading: false,
    data: {
      page: 1,
      pages: 1,
      newsList: [],
    },
  },
  action: {
    type: string;
    payload: responseType;
  }
) => {
  switch (action.type) {
    case INIT:
      return {
        ...state,
        loading: true,
      };
    case SUCCESS:
      return {
        loading: false,
        data: action.payload,
      };
    default:
      return state;
  }
};
