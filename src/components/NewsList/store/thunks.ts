import { actions } from "./actions";
import { API } from "../api/api";
export const getData = (dispatch: any, page: number) => {
  actions.init(dispatch);
  API.getNewsList({ page }).then((response) => {
    actions.success(dispatch, response);
  });
};
