export const INIT = "INIT";
export const SUCCESS = "SUCCESS";

export type newsListType = Array<any>;
export interface responseType {
  page: number;
  pages: number;
  newsList: newsListType;
}

export interface newsListProps {
  page: number;
}
export type newsListResponse = Promise<{
  page: number;
  newsList: Array<any>;
  pages: number;
}>;
