import { newsListProps, newsListResponse } from "../store/types";

export const API = {
  getNewsList: ({ page }: newsListProps): newsListResponse =>
    new Promise((resolve) => {
      setTimeout(() => {
        return resolve({
          page: page,
          pages: 100,
          newsList: Array.from({ length: 10 }, (_, i) => {
            const itemId = page * 10 - 10 + i;
            return {
              id: itemId,
              title: "Lorem ipsum title " + itemId,
              description: `Lorem ipsum story about № ${itemId} dolor sit amet, consectetur adipiscing elit. Aenean et feugiat justo, nec pharetra odio. Aenean sagittis, enim a ornare congue...`,
            };
          }),
        });
      }, 1000);
    }),
};
