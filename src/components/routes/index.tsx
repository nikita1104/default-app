import React from "react";
import { Switch, Route } from "react-router-dom";
import { Main } from "../Main";
import { NewsList } from "../NewsList";
import { NewsItem } from "../NewsItem";
import { Login } from "../Login";
import ProtectedRoute from "./ProtectedRoute";
import ProtectedAuthRoute from "./ProtectedAuthRoute";

export const Routes = () => {
  return (
    <>
      <Switch>
        <Route
          exact
          path="/login"
          render={(props: any) => (
            <ProtectedAuthRoute
              redirectTo={props?.redirectState?.pathToGo}
              onRender={<Login />}
            />
          )}
        />
        {[
          { path: "/", component: <Main /> },
          {
            path: "/news-list",
            component: <NewsList />,
          },
          {
            path: "/news/:id",
            component: <NewsItem />,
          },
        ].map((route, i) => (
          <Route
            key={i}
            exact
            path={route.path}
            render={() => (
              <ProtectedRoute
                onRender={route.component}
                pathToGo={route.path}
              />
            )}
          />
        ))}
      </Switch>
    </>
  );
};
