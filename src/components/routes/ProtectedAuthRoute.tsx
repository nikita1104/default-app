import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

const ProtectedAuthRoute = ({ auth, login, redirectTo, onRender }: any) => {
  const localStorageToken = localStorage.getItem("token");

  useEffect(() => {
    if (localStorageToken !== null) {
      login(localStorageToken);
    }
  }, [login, localStorageToken]);
  if (typeof auth.token !== "undefined") {
    return (
      <Redirect
        to={{
          pathname: redirectTo ? redirectTo : "/",
        }}
      />
    );
  } else {
    return <>{onRender}</>;
  }
};

export default connect(
  (store: any) => ({
    auth: store.auth,
  }),
  (dispatch) => ({
    login: (token: string) => dispatch({ type: "LOGIN", payload: { token } }),
  })
)(ProtectedAuthRoute);
