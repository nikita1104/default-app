import React, { useEffect } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

const ProtectedRoute = ({ store, login, pathToGo, onRender }: any) => {
  const localStorageToken = localStorage.getItem("token");
  useEffect(() => {
    if (localStorageToken !== null) {
      login({ token: localStorageToken });
    }
  }, [login, localStorageToken]);
  if (typeof store.auth.token === "undefined" && localStorageToken === null) {
    return (
      <Redirect
        to={{
          pathname: "/login",
          state: { pathToGo: pathToGo },
        }}
      />
    );
  } else {
    return onRender;
  }
};

export default connect(
  (store: any) => ({
    store,
  }),
  (dispatch) => ({
    login: (token: string) => dispatch({ type: "LOGIN", payload: token }),
  })
)(ProtectedRoute);
