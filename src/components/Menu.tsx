import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

export const Menu = () => (
  <Styled>
    {[
      { path: "/", label: "Main" },
      { path: "/news-list", label: "News List" },
    ].map((item, i) => (
      <Link to={item.path} className="menu-item" key={i}>
        {item.label}
      </Link>
    ))}
  </Styled>
);

const Styled = styled.div`
  .menu-item {
    margin-right: 10px;
  }
`;
