import validator from "validator";

export const validation = {
  email: (value: string | undefined) => {
    if (typeof value === "undefined") return "Required";
    if (!validator.isEmail(value)) return "Enter valid email";
    return undefined;
  },
  password: (value: string | undefined) => (value ? undefined : "Required"),
};
