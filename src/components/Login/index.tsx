import React from "react";
import { connect } from "react-redux";
import { Form } from "react-final-form";
import { validation } from "./validation";
import { API } from "../../api/index";
import { TextFIeld } from "./fields/TextField";

export const Component = (props: any) => {
  return (
    <>
      <h1>Login</h1>
      <Form
        onSubmit={props.login}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <div>
              <TextFIeld
                name="email"
                validate={validation.email}
                label="Email"
                type="text"
              />
            </div>
            <div>
              <TextFIeld
                name="password"
                validate={validation.password}
                label="Password"
                type="password"
              />
            </div>
            <button type="submit">Submit</button>
          </form>
        )}
      />
    </>
  );
};

const mapDIspatchToProps = (
  dispatch: (arg0: { type: string; payload: any }) => any
) => {
  return {
    login: (creds: { email: string; password: string }) =>
      API.login(creds).then((resp) => {
        dispatch({ type: "LOGIN", payload: resp });
      }),
  };
};

export const Login = connect(undefined, mapDIspatchToProps)(Component);
