import React from "react";
import { textFeldPropsTypes } from "./types";
import { Field } from "react-final-form";

export const TextFIeld = ({
  type,
  name,
  label,
  validate,
}: textFeldPropsTypes) => (
  <Field name={name} validate={validate}>
    {({ input, meta }) => (
      <div>
        <input {...input} type={type} placeholder={label} />
        {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
    )}
  </Field>
);
