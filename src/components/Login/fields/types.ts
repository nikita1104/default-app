export interface textFeldPropsTypes {
  type: string;
  name: string;
  label: string;
  validate?: any;
}
