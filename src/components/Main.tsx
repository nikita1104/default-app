import React from "react";

export const Main = () => (
  <div id="main-page">
    <h1>Welcome to SiteName!</h1>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sed
      fringilla sapien. Interdum et malesuada fames ac ante ipsum primis in
      faucibus. In et nibh metus. Pellentesque ultrices euismod risus nec
      bibendum. Cras in mauris interdum, vestibulum ante a, pellentesque ipsum.
      Suspendisse vel tellus dignissim, lobortis velit vitae, placerat nisi.
      Cras imperdiet tristique dignissim. Etiam euismod vel est at facilisis.
      Nullam sapien mi, porttitor ac convallis vel, consectetur eu quam. Donec
      eros ex, varius sed efficitur ut, luctus a orci. Fusce enim est, egestas
      eleifend pharetra in, luctus vitae nisi. Vivamus ultrices varius semper.
      Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
      cubilia curae; In hac habitasse platea dictumst. Morbi ac vestibulum
      risus, volutpat congue enim.
    </p>
    <p>
      Morbi nec pellentesque eros. Donec ac sollicitudin tortor, non molestie
      tellus. Sed mattis suscipit urna nec blandit. Sed arcu orci, vestibulum
      sit amet libero ut, vulputate eleifend libero. Suspendisse auctor ligula
      justo, in sollicitudin nibh eleifend eu. Phasellus porta quam purus,
      aliquet dapibus tortor fringilla vel. Nam sit amet turpis nec nunc sodales
      ultrices. Duis vestibulum venenatis sapien. Nam gravida dictum tellus,
      vulputate condimentum metus accumsan iaculis. In sapien massa, aliquam
      eget vehicula ut, varius a sapien. Donec et efficitur ex. Fusce interdum
      odio et dolor hendrerit, ut tincidunt turpis laoreet. Aliquam ut tortor et
      mauris fermentum tincidunt sit amet id elit.
    </p>
    <p>
      Nullam lorem turpis, fermentum sit amet dui sit amet, fringilla venenatis
      tortor. Vivamus feugiat, diam ac varius efficitur, augue neque ullamcorper
      elit, sed sodales enim sapien vel ante. Sed nec diam eget urna lobortis
      porta. Cras justo lorem, fermentum a tincidunt quis, tincidunt non massa.
      Morbi finibus placerat tellus ornare congue. Nunc dolor neque, pretium sed
      est at, pretium molestie quam. Nulla ultricies metus justo, non dignissim
      eros tempor nec. Aliquam id molestie arcu. Vivamus in ante volutpat,
      posuere eros in, consequat magna.
    </p>
    <p>
      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
      inceptos himenaeos. Etiam augue enim, facilisis a erat at, finibus
      vulputate massa. Mauris vel pulvinar metus. Sed et ultrices ex, id
      interdum sem. Pellentesque iaculis urna eget tellus mollis, vel faucibus
      lacus dignissim. In in mauris malesuada, faucibus elit eget, sodales ante.
      Donec lacinia vulputate viverra. Aliquam eleifend urna vel enim
      condimentum viverra. Orci varius natoque penatibus et magnis dis
      parturient montes, nascetur ridiculus mus. Orci varius natoque penatibus
      et magnis dis parturient montes, nascetur ridiculus mus. Integer
      vestibulum quam vitae tortor tincidunt, eu eleifend lacus lacinia. Cras in
      neque mi. Integer aliquam leo in eros faucibus, at congue mi sagittis.
    </p>
    <p>
      Etiam blandit odio non luctus maximus. Fusce leo purus, sodales non
      sollicitudin ut, semper quis enim. Ut maximus egestas felis eu venenatis.
      Aenean tincidunt cursus augue, at porttitor lectus lobortis ac. Praesent
      nec ligula a ex mattis posuere. Suspendisse potenti. Nunc cursus ultrices
      lorem nec tempor. Nam non urna nec purus cursus sagittis. Mauris semper
      nibh mollis justo tristique porta. Quisque scelerisque urna erat, a
      sagittis quam vestibulum et. Pellentesque in fermentum risus. Nam accumsan
      sit amet magna non egestas. Nam fermentum felis id mollis commodo. Nam
      quis dignissim tortor. Pellentesque sapien sem, convallis sit amet enim
      ut, dapibus dictum nisl.
    </p>
  </div>
);
