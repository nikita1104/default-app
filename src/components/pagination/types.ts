export interface paginationPropsTypes {
  pageCount: number;
  marginPagesDisplayed: number;
  pageRangeDisplayed: number;
  onPageChange: any;
  forcePage?: number;
}
