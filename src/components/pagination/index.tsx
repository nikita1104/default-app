import React from "react";
import ReactPaginate from "react-paginate";
import styled from "styled-components";
import { paginationPropsTypes } from "./types";

export const Pagination = ({
  pageCount,
  marginPagesDisplayed,
  pageRangeDisplayed,
  onPageChange,
  forcePage = 1,
}: paginationPropsTypes) => (
  <Styled>
    <ReactPaginate
      previousLabel={"previous"}
      nextLabel={"next"}
      breakLabel={"..."}
      breakClassName={"break-me"}
      pageCount={pageCount}
      marginPagesDisplayed={marginPagesDisplayed}
      pageRangeDisplayed={pageRangeDisplayed}
      onPageChange={(x) => onPageChange(x.selected + 1)}
      containerClassName={"pagination"}
      activeClassName={"active"}
      forcePage={forcePage - 1}
    />
  </Styled>
);

const Styled = styled.div`
  .pagination {
    display: flex;
    li {
      &.active {
        color: red;
      }
      :not(.active) {
        a {
          cursor: pointer;
        }
      }
      .disabled {
        cursor: no-drop;
        opacity: 0.5;
      }
      display: block;
      margin: 0 3px;
    }
    a {
      display: block;
      padding: 5px;
    }
  }
`;
