import React from "react";
import spinnerSrc from "./spinner.svg";
import styled from "styled-components";

export const Preload = () => (
  <Styled>
    <img src={spinnerSrc} alt="spinner" />
  </Styled>
);
const Styled = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background: rgba(238, 238, 238, 0.62);
`;
