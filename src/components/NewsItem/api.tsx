import { newsItemProps, newsItemResponse } from "./store/types";

export const API = {
  getNewsItem: ({ news_id }: newsItemProps): newsItemResponse =>
    new Promise((resolve) => {
      setTimeout(() => {
        return resolve({
          title: `Title of news #${news_id}`,
          content: `Content of news #${news_id}`,
        });
      }, 1000);
    }),
};
