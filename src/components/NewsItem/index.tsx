import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import { getData } from "./store/thunks";
import { Preload } from "../UI";

export const Component = ({ store, get_news_item }: any) => {
  const { id } = useParams();
  const { data, loading } = store;
  useEffect(() => {
    get_news_item(id);
  }, [id, get_news_item]);
  if (loading) return <Preload />;
  if (data)
    return (
      <>
        <h1>{data.title}</h1>
        <p>{data.content}</p>
      </>
    );
  return <></>;
};

export const NewsItem = connect(
  (store: any) => ({ store: store.newsItem }),
  (dispatch) => ({
    get_news_item: (news_id: string) => {
      getData(dispatch, news_id);
    },
  })
)(Component);
