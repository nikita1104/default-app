import { actions } from "./actions";
import { API } from "../api";

export const getData = (dispatch: any, news_id: string) => {
  actions.init(dispatch);
  API.getNewsItem({ news_id }).then((response) => {
    actions.success(dispatch, response);
  });
};
