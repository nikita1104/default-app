import { GET_NEWS_ITEM, INIT, SUCCESS } from "./types";

export const newsItemsReducer = (
  state: {
    loading: boolean;
    data?: {
      title: string;
      content: string;
    };
  } = {
    loading: false,
  },
  action: {
    type: string;
    payload: { page: number; pages: number; data: Array<any> };
  }
) => {
  switch (action.type) {
    case INIT:
      return {
        ...state,
        loading: true,
      };
    case SUCCESS:
      return {
        loading: false,
        data: action.payload,
      };
    case GET_NEWS_ITEM:
      return action.payload;
    default:
      return state;
  }
};
