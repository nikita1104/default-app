export const GET_NEWS_ITEM = "GET_NEWS_ITEM";

export const INIT = "INIT";
export const SUCCESS = "SUCCESS";

export interface newsItemProps {
  news_id: string;
}

export type newsItemResponse = Promise<{
  title: string;
  content: string;
}>;
