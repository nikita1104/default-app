import { INIT, SUCCESS } from "./types";

export const actions = {
  init: (dispatch: any) =>
    dispatch({
      type: INIT,
    }),
  success: (dispatch: any, payload: any) =>
    dispatch({
      type: SUCCESS,
      payload,
    }),
};
