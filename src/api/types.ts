export interface loginProps {
  email: string;
  password: string;
}
export type loginResponse = Promise<{
  token: string;
  user: {
    name: string;
  };
}>;
