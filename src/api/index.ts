import { loginProps, loginResponse } from "./types";

export const API = {
  login: ({ email, password }: loginProps): loginResponse =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          token: "foo",
          user: {
            name: "Ivan",
          },
        });
      }, 1000);
    }),
};
